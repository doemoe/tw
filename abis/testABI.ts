import {ethers} from 'ethers';

export const TestABI = new ethers.utils.Interface([
  // Reads

  // Writes
  'function mint(uint8 _mintAmount) external payable',
  'function traverseChains(uint16 _chainId, uint tokenId) public payable',

  // Events
]);

export default TestABI;
