const routes = {
  home: '/',
  story: '/#story',
  roadmap: '/#roadmap',
  faq: '/#faq',
  team: '/#team',
  mint: '/mint',
  market: '/market',
};

export default routes;
