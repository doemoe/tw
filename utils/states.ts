export type MintState = {
  amount: number;
  minting?: boolean;
  confirming?: boolean;
  hash?: string;
  error?: Error;
};
