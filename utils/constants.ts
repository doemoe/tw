interface ChainItem {
  chainID: number;
  displayName: string;
}

// /* Chain IDs */
// export const ETHEREUM_CHAIN_ID = 1;
// export const BNB_CHAIN_ID = 56;
// export const POLYGON_CHAIN_ID = 137;
// export const FANTOM_CHAIN_ID = 250;
// export const AVALANCHE_CHAIN_ID = 43114;

// /* Display Names of each Chain */
// export const ETHEREUM_DISPLAY_NAME = "Ethereum";
// export const BNB_CHAIN_DISPLAY_NAME = "BNB Chain";
// export const POLYGON_DISPLAY_NAME = "Polygon";
// export const FANTOM_DISPLAY_NAME = "Fantom";
// export const AVALANCHE_DISPLAY_NAME = "Avalanche";

// export const supportedChainList: Array<ChainItem> = [
//    {chainID: 1, displayName: "Ethereum"},
//    {chainID: 56, displayName: "BNB Chain"},
//    {chainID: 137, displayName: "Polygon"},
//    {chainID: 250, displayName: "Fantom"},
//    {chainID: 43114, displayName: "Avalanche"},
// ];

export const supportedChain = {
  1: {name: 'Ethereum', color: '#8b8b8b', symbol: 'ETH', token: '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2', price: '0.01'},
  56: {name: 'BNB Chain', color: '#f0b90b', symbol: 'BNB', token: '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c', price: '0.07'},
  137: {name: 'Polygon', color: '#8247e5', symbol: 'MATIC', token: '0x0d500b1d8e8ef31e21c99d1db9a6444d3adf1270', price: '20'},
  // 137 : {name: "Polygon", color: "#8247e5", symbol: "MATIC", price: "1"},
  250: {name: 'Fantom', color: '#13b5ec', symbol: 'FTM', token: '0x21be370d5312f44cb42ce377bc9b8a0cef1a4c83', price: '20'},
  43114: {name: 'Avalanche', color: '#e84142', symbol: 'AVAX', token: '0xb31f66aa3c1e785363f0875a1b74e27b85fd66c7', price: '0.3'},
};

export const contractAddress = '0xc907eA25e5183EaA41D4737421928163174d81CE';
