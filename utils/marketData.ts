export const opensea = [
  {chain: 'ETH', link: 'https://opensea.io/collection/titanworld'},
  {chain: 'Polygon', link: 'https://opensea.io/collection/titanworld-polygon'},
];

export const looksrare = [
  {
    chain: 'ETH',
    link: 'https://looksrare.org/collections/0xc907eA25e5183EaA41D4737421928163174d81CE',
  },
];

export const tofunft = [
  {chain: 'ETH', link: 'https://tofunft.com/collection/titan-world-eth/items'},
  {
    chain: 'BNB Chain(BSC)',
    link: 'https://tofunft.com/collection/titan-world-bsc/items',
  },
  {
    chain: 'Polygon',
    link: 'https://tofunft.com/collection/titan-world-polygon/items',
  },
  {
    chain: 'Avalanche',
    link: 'https://tofunft.com/collection/titan-world-avax/items',
  },
  {chain: 'Fantom', link: 'https://tofunft.com/collection/titan-world/items'},
];
