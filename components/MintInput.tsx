import React, {memo, useState} from 'react';

import {useContract} from 'wagmi';

import {Box, Button, VStack, Flex, Input, Img} from '@chakra-ui/react';

const MintInput = (props) => {
  const {
    value,
    confirming,
    minting,
    handleChange,
    handleIncrement,
    handleDecrement,
  } = props;

  return (
    <Flex justifyContent="center">
      <Box
        as="button"
        w="3ch"
        disabled={confirming || minting}
        onClick={handleDecrement}
      >
        -
      </Box>
      <Input
        maxW="120px"
        mx="2rem"
        textAlign="center"
        maxLength={4}
        value={value}
        isDisabled={confirming || minting}
        onChange={handleChange}
      ></Input>
      <Box
        as="button"
        w="3ch"
        disabled={confirming || minting}
        onClick={handleIncrement}
      >
        +
      </Box>
    </Flex>
  );
};

export default MintInput;
