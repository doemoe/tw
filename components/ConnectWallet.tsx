import React, {memo, useEffect} from 'react';

import {useConnect, useAccount, useNetwork} from 'wagmi';
import {useIsMounted} from 'hooks/useIsMounted';

import {supportedChain} from 'utils/constants';
import {
  Stack,
  Button,
  Text,
  Box,
  Menu,
  Img,
  MenuButton,
  MenuList,
  MenuItem,
} from '@chakra-ui/react';

import styles from './ConnectWallet.module.scss';

const ConnectWallet = (props) => {
  const {isMobile} = props;
  const isMounted = useIsMounted();
  const [{data: connectData, error: connectError}, connect] = useConnect();
  const [{data: accountData}, disconnect] = useAccount({
    fetchEns: true,
  });
  const [
    {data: networkData, error: networkError, loading: networkLoading},
    switchNetwork,
  ] = useNetwork();

  return (
    <Stack as="div" spacing={4} direction="row" align="center" my={4}>
      {connectData.connected ? (
        <>
          {!isMobile && (
            <Box display="inline-block" ml="16px">
              {accountData.ens?.name
                ? `${accountData.ens?.name}`
                : `${accountData.address.substring(0, 7)}...`}
            </Box>
          )}
          <Menu>
            <MenuButton
              as={Button}
              colorScheme={
                supportedChain[networkData.chain?.id] ? null : 'yellow'
              }
              width="130px"
              alignItems="center"
            >
              <Text className={styles.ButtonText}>
                {supportedChain[networkData.chain?.id]
                  ? supportedChain[networkData.chain?.id].name
                  : 'Unsupported'}
              </Text>
            </MenuButton>
            <MenuList width="200px" textAlign="center">
              <MenuItem
                onClick={async () => await switchNetwork(1)}
                display="flex"
                justifyContent="space-between"
              >
                <Text className={styles.MenuItemText}>Ethereum</Text>
                <Img src="/static/ethereum.svg" height="2rem" px="6px" />
              </MenuItem>
              <MenuItem
                onClick={async () => {
                  try {
                    await window.ethereum.request({
                      method: 'wallet_switchEthereumChain',
                      params: [{chainId: '0x38'}],
                    });
                  } catch (switchError) {
                    if (switchError.code === 4902) {
                      try {
                        await window.ethereum.request({
                          method: 'wallet_addEthereumChain',
                          params: [
                            {
                              chainId: '0x38',
                              chainName: 'Binance Smart Chain Mainnet',
                              nativeCurrency: {
                                name: 'BNB',
                                symbol: 'BNB',
                                decimals: 18,
                              },
                              rpcUrls: ['https://bsc-dataseed1.binance.org/'],
                              blockExplorerUrls: ['https://bscscan.com/'],
                            },
                          ],
                        });
                      } catch (addError) {
                        // handle "add" error
                      }
                    }
                    // handle other "switch" errors
                  }
                }}
                display="flex"
                justifyContent="space-between"
              >
                <Text className={styles.MenuItemText}>BNB Chain</Text>
                <Img src="/static/bnbChain.svg" height="2rem" />
              </MenuItem>
              <MenuItem
                onClick={async () => {
                  try {
                    await window.ethereum.request({
                      method: 'wallet_switchEthereumChain',
                      params: [{chainId: '0x89'}],
                    });
                  } catch (switchError) {
                    if (switchError.code === 4902) {
                      try {
                        await window.ethereum.request({
                          method: 'wallet_addEthereumChain',
                          params: [
                            {
                              chainId: '0x89',
                              rpcUrls: ['https://rpc-mainnet.matic.network/'],
                              chainName: 'Polygon Mainnet',
                              nativeCurrency: {
                                name: 'MATIC',
                                symbol: 'MATIC',
                                decimals: 18,
                              },
                              blockExplorerUrls: ['https://polygonscan.com/'],
                            },
                          ],
                        });
                      } catch (addError) {
                        // handle "add" error
                      }
                    }
                    // handle other "switch" errors
                  }
                }}
                display="flex"
                justifyContent="space-between"
              >
                <Text className={styles.MenuItemText}>Polygon</Text>
                <Img src="/static/polygon.svg" height="2rem" paddingTop="6px" />
              </MenuItem>
              <MenuItem
                onClick={async () => {
                  try {
                    await window.ethereum.request({
                      method: 'wallet_switchEthereumChain',
                      params: [{chainId: '0xA86A'}],
                    });
                  } catch (switchError) {
                    if (switchError.code === 4902) {
                      try {
                        await window.ethereum.request({
                          method: 'wallet_addEthereumChain',
                          params: [
                            {
                              chainId: '0xA86A',
                              chainName: 'Avalanche C-Chain',
                              nativeCurrency: {
                                name: 'AVAX',
                                symbol: 'AVAX',
                                decimals: 18,
                              },
                              rpcUrls: [
                                'https://api.avax.network/ext/bc/C/rpc',
                              ],
                              blockExplorerUrls: ['https://snowtrace.io'],
                            },
                          ],
                        });
                      } catch (addError) {
                        // handle "add" error
                      }
                    }
                    // handle other "switch" errors
                  }
                }}
                display="flex"
                justifyContent="space-between"
              >
                <Text className={styles.MenuItemText}>Avalanche</Text>
                <Img src="/static/avalanche.svg" height="2rem" />
              </MenuItem>
              <MenuItem
                onClick={async () => {
                  try {
                    await window.ethereum.request({
                      method: 'wallet_switchEthereumChain',
                      params: [{chainId: '0xFA'}],
                    });
                  } catch (switchError) {
                    if (switchError.code === 4902) {
                      try {
                        await window.ethereum.request({
                          method: 'wallet_addEthereumChain',
                          params: [
                            {
                              chainId: '0xFA',
                              chainName: 'Fantom Opera',
                              nativeCurrency: {
                                name: 'Fantom',
                                symbol: 'FTM',
                                decimals: 18,
                              },
                              rpcUrls: ['https://rpc.ftm.tools'],
                              blockExplorerUrls: ['https://ftmscan.com/'],
                            },
                          ],
                        });
                      } catch (addError) {
                        // handle "add" error
                      }
                    }
                    // handle other "switch" errors
                  }
                }}
                display="flex"
                justifyContent="space-between"
              >
                <Text className={styles.MenuItemText}>Fantom</Text>
                <Img src="/static/fantom.svg" height="2rem" />
              </MenuItem>
            </MenuList>
          </Menu>
          {/* <div>Connected to {accountData.connector.name}</div> */}
          {!isMobile && (
            <Button
              colorScheme="red"
              onClick={disconnect}
              className={styles.ButtonText}
            >
              Disconnect
            </Button>
          )}
        </>
      ) : (
        connectData.connectors.map((connector) => (
          <Button
            // disabled={!connector.ready}
            disabled={isMounted ? !connector.ready : false}
            key={connector.id}
            onClick={() => connect(connector)}
          >
            {isMounted
              ? 'Connect Wallet'
              : connector.id === 'injected'
              ? connector.id
              : connector.name}
            {/* {!connector.ready && ' (unsupported)'} */}
            {isMounted ? !connector.ready && ' (unsupported)' : ''}
          </Button>
        ))
      )}
    </Stack>
  );
};

export default ConnectWallet;
