import React, {memo} from 'react';
import NextLink from 'next/link';
import NextHead from 'next/head';

import {
  LinkBox,
  LinkOverlay,
  Box,
  Flex,
  Heading,
  Text,
  Container,
  Button,
  Link,
  VStack,
  Img,
} from '@chakra-ui/react';
import {withPublicLayout} from 'layouts';

import {opensea, looksrare, tofunft} from 'utils/marketData';

const MarketItem = (props) => {
  const {svgSrc, marketName, data} = props;
  console.log(data);

  return (
    <>
      <NextHead>
        <title>Titan World | Marketplaces</title>
        <meta
          property="og:title"
          content="Titan World | Marketplaces"
          key="markettitle"
        />
        <meta
          property="twitter:title"
          content="Titan World | Marketplaces"
          key="mktitle"
        />
      </NextHead>
      <VStack
        w="full"
        maxW={['md', 'xl']}
        p="3"
        rounded="md"
        spacing="1rem"
        top="0"
        position="relative"
        color="lightgray"
        boxShadow="0 2px 2px 0 rgba(255,255,255,0.14), 0 1px 5px 0 rgba(255,255,255,0.12), 0 3px 1px -2px rgba(255,255,255,0.2)"
        transition="all 0.3s ease"
        _hover={{
          color: 'white',
          top: '-3px',
          boxShadow:
            '0 16px 24px 2px rgba(255,255,255,0.14), 0 6px 30px 5px rgba(255,255,255,0.12), 0 8px 10px -5px rgba(255,255,255,0.3)',
        }}
      >
        <Flex>
          <Img src={`${svgSrc}`} h="2rem" />
          <Text
            pl="1rem"
            fontSize="xl"
            fontWeight="700"
          >{`${marketName}`}</Text>
        </Flex>
        <Flex flexWrap="wrap" justifyContent="center">
          {data.map((item) => {
            const {chain, link} = item;
            return (
              <NextLink href={`${link}`} key={`${link}`} passHref>
                <Button
                  as="a"
                  target="_blank"
                  m="5px"
                  w="15ch"
                  variant="outline"
                >
                  {`${chain}`}
                </Button>
              </NextLink>
            );
          })}
        </Flex>
      </VStack>
    </>
  );
};

const Market = memo(() => {
  return (
    <Container w={['xs', 'sm', 'md', 'xl']}>
      <VStack textAlign="center" spacing="1.5rem">
        <MarketItem
          svgSrc="/static/opensea.svg"
          marketName="Opensea"
          data={opensea}
        />
        <MarketItem
          svgSrc="/static/looksrare.svg"
          marketName="LooksRare"
          data={looksrare}
        />
        <MarketItem
          svgSrc="/static/tofunft.svg"
          marketName="tofuNFT"
          data={tofunft}
        />
      </VStack>
    </Container>
  );
});

export default withPublicLayout(Market);
