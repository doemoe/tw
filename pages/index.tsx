import type {NextPage} from 'next';
import NextLink from 'next/link';
import NextHead from 'next/head';
import React, {useEffect, useState} from 'react';
// import Head from 'next/head';
// import Image from 'next/image';
// import styles from '../styles/Home.module.css';
import {useConnect} from 'wagmi';
import {useIsMounted} from 'hooks/useIsMounted';
import {useMediaQuery} from '@chakra-ui/react';

import {
  Box,
  Container,
  Stack,
  Input,
  Button,
  Heading,
  Text,
  VStack,
  Img,
  Flex,
  Link,
  Accordion,
  AccordionButton,
  AccordionPanel,
  AccordionItem,
  AccordionIcon,
} from '@chakra-ui/react';

import styles from './index.module.scss';
import {withPublicLayout} from 'layouts';
import routes from 'utils/routes';

const Home: NextPage = () => {
  const [scan, setScan] = useState(false);
  // scrollUpButton
  const [showButton, setShowButton] = useState(false);
  const [isMobile] = useMediaQuery('(max-width: 992px)');

  useEffect(() => {
    window.addEventListener('scroll', () => {
      if (window.pageYOffset > 600) {
        setShowButton(true);
      } else {
        setShowButton(false);
      }
    });
  }, []);

  // This function will scroll the window to the top
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth', // for smoothly scrolling
    });
  };

  return (
    <>
      <NextHead>
        <title>Titan World | Home</title>
        <meta
          property="og:title"
          content="Titan World | Home"
          key="hometitle"
        />
        <meta
          property="twitter:title"
          content="Titan World | Home"
          key="twtitle"
        />
      </NextHead>
      {showButton && (
        <button onClick={scrollToTop} className={styles.backToTop}>
          Go Top
        </button>
      )}
      <VStack>
        <Text
          fontSize={['1.5em', '2em', '3em', '4em']}
          fontFamily="Segoe UI"
          fontWeight="700"
          className={styles.movegradient}
        >
          Traverse Omnichain.
        </Text>

        {/* Banner */}
        <Box marginTop="8px" id="banner">
          <Img
            width="full"
            src="/static/img/realTitanWorld.webp"
            className={styles.banner}
          />
        </Box>

        {/* Story */}
        <Container textAlign="center" maxW="2xl" id="story">
          <Box my="10px" id="story">
            <Heading as="h2" py="5px" fontSize={['1.5em', '2em', '3em']}>
              The Story
            </Heading>
            <Text lineHeight="1.5rem">
              Our story begins in the cave, where a giant sere palm stands
              against the wall with loads of spoils. Skeletons are scattered
              everywhere on the ground. Our way is blocked by a huge human
              skeleton, some words were engraved on it, Titan World.
            </Text>
            <Box my="1rem">
              <Img
                src="/static/img/mural_1.webp"
                cursor="help"
                display={scan ? 'none' : ''}
                onClick={() => setScan(true)}
              />
              <Img
                src="/static/img/mural_2.webp"
                display={scan ? '' : 'none'}
              />
            </Box>
            <Text lineHeight="1.5rem">
              Fuzzy murals could be seen on the burned wall. Something can be
              discerned after adventurers&apos; scanning, like skulls, giant
              palms, weapons and roaring giants.
            </Text>
          </Box>
        </Container>

        {/* Roadmap */}
        {!isMobile ? (
          <Container maxW="4xl">
            <Box my="10px" textAlign="center" id="roadmap">
              <Heading as="h2" py="5px" fontSize={['1.5em', '2em', '3em']}>
                Roadmap
              </Heading>
            </Box>
            <Box className={styles.cardWrapper}>
              <Box className={styles.cardRow}>
                <Box
                  className={`${styles.card} ${styles.leftCard} ${styles.firstCard}`}
                >
                  <Heading as="h4" size="md" paddingBottom="10px">
                    Step 0: Origin
                  </Heading>
                  <Text>
                    Airdrop some of our NFTs for free as a gift to expand
                    awareness of omnichain NFTs.
                  </Text>
                </Box>
              </Box>
              <Box className={styles.cardRow}>
                <Box className={`${styles.card} ${styles.rightCard}`}>
                  <Heading as="h4" size="md" paddingBottom="10px">
                    Step 1: Hands
                  </Heading>
                  <Text>
                    Launch of the first series of Titan Hand NFT in Mechanical
                    styles on Ethereum. BNB Chain/BSC (Beast) → Fantom (Human) →
                    Avalanche (Skull) → Polygon (Mummy) in succession.
                  </Text>
                </Box>
              </Box>
              <Box className={styles.cardRow}>
                <Box className={`${styles.card} ${styles.leftCard}`}>
                  <Heading as="h4" size="md" paddingBottom="10px">
                    Step 2: Equipments
                  </Heading>
                  <Text>
                    Launch of Titan Weapon and Titan Armor on the basis of Titan
                    Hand.
                  </Text>
                </Box>
              </Box>
              <Box className={styles.cardRow}>
                <Box className={`${styles.card} ${styles.rightCard}`}>
                  <Heading as="h4" size="md" paddingBottom="10px">
                    Step 3: Resurrection
                  </Heading>
                  <Text>
                    By leveraging the magical &quot;NFT Assembling&quot; (to be
                    announced), you are enabled to combine Titan Hand, Titan
                    Weapon and Titan Armor into one NFT, namely Titan itself,
                    accomplishing the resurrection. In this way, we built the
                    Titan World together.
                  </Text>
                </Box>
              </Box>
              <Box className={styles.cardRow}>
                <Box className={`${styles.card} ${styles.leftCard}`}>
                  <Heading as="h4" size="md" paddingBottom="10px">
                    Step 4: Future
                  </Heading>
                  <Text>
                    Release Titan World game, every adventure who owns a
                    complete Titan NFT can rule a piece of land in the Titan
                    World.
                  </Text>
                </Box>
              </Box>
            </Box>
          </Container>
        ) : (
          <Box className="placeholder"></Box>
        )}
        {/* FAQ */}
        <Container maxW="3xl" mt="20px">
          <Box my="10px" textAlign="center" id="faq">
            <Heading as="h2" py="5px" fontSize={['1.5em', '2em', '3em']}>
              FAQ
            </Heading>
          </Box>
          <Accordion allowMultiple>
            <AccordionItem>
              <AccordionButton>
                <Box flex="1" textAlign="left" className={styles.AccordionText}>
                  What is Titan World?
                </Box>
                <AccordionIcon />
              </AccordionButton>
              <AccordionPanel pb={4}>
                Titan World is a NFT project focusing on omnichain and NFT
                Assembling.
              </AccordionPanel>
            </AccordionItem>
            <AccordionItem>
              <AccordionButton>
                <Box flex="1" textAlign="left" className={styles.AccordionText}>
                  What are Omnichain NFTs?
                </Box>
                <AccordionIcon />
              </AccordionButton>
              <AccordionPanel pb={4}>
                Omnichain NFTs are chain-agnostic NFTs that can be transferred
                between EVM chains, enabling much more accessibility to future
                integration with the rest of the Web3 space.
              </AccordionPanel>
            </AccordionItem>
            <AccordionItem>
              <AccordionButton>
                <Box flex="1" textAlign="left" className={styles.AccordionText}>
                  How do you achieve this?
                </Box>
                <AccordionIcon />
              </AccordionButton>
              <AccordionPanel pb={4}>
                The project is built on LayerZero Protocol. Simply put,
                LayerZero is a transport layer for messaging that allows smart
                contracts to easily communicate among many or any blockchains,
                developed by LayerZero Labs based in Canada.
              </AccordionPanel>
            </AccordionItem>
            <AccordionItem>
              <AccordionButton>
                <Box flex="1" textAlign="left" className={styles.AccordionText}>
                  How to transfer my NFTs to another chain?
                </Box>
                <AccordionIcon />
              </AccordionButton>
              <AccordionPanel pb={4}>
                <Text>Transfer to another chain is quite simple.</Text>
                <Text>
                  Our collections on{' '}
                  <NextLink href="https://tofunft.com" passHref>
                    <Link isExternal color="#0b70db">
                      tofuNFT.com
                      <Img
                        src="/static/externalLink.svg"
                        h="1rem"
                        display="inline-block"
                        mx="2px"
                      />
                    </Link>
                  </NextLink>{' '}
                  have been granted native cross-chain transfer support. You can
                  navigate to the detail page of your NFT, click
                  &quot;Traverse&quot; button and follow the instructions.
                </Text>
              </AccordionPanel>
            </AccordionItem>
            <AccordionItem>
              <AccordionButton>
                <Box flex="1" textAlign="left" className={styles.AccordionText}>
                  What about the mint price?
                </Box>
                <AccordionIcon />
              </AccordionButton>
              <AccordionPanel pb={4}>
                It depends on the target chain.
                <Text>-Titan Hand series-</Text>
                <Text>Ethereum: 0.01 ETH</Text>
                <Text>Polygon: 20 MATIC</Text>
                <Text>BNB Chain: 0.07 BNB</Text>
                <Text>Avalanche: 0.3 AVAX</Text>
                <Text>Fantom: 20 FTM</Text>
              </AccordionPanel>
            </AccordionItem>
            <AccordionItem>
              <AccordionButton>
                <Box flex="1" textAlign="left" className={styles.AccordionText}>
                  What is the total supply?
                </Box>
                <AccordionIcon />
              </AccordionButton>
              <AccordionPanel pb={4}>
                -Titan Hand series-
                <Text>Ethereum: 2048</Text>
                <Text>Polygon: 1024</Text>
                <Text>BNB Chain: 1024</Text>
                <Text>Avalanche: 1024</Text>
                <Text>Fantom: 1024</Text>
              </AccordionPanel>
            </AccordionItem>
          </Accordion>
        </Container>

        {/* Team */}
        <Container maxW="7xl">
          <Box mt="20px" mb="10px" textAlign="center" id="team">
            <Heading as="h2" py="5px" fontSize={['1.5em', '2em', '3em']}>
              Team
            </Heading>
          </Box>
          <Flex justifyContent="space-around" wrap="wrap">
            <Box textAlign="center" m="10px">
              <Img src="/static/img/team_1.webp" h="250px" />
              <Text fontSize={['20px', '24px']} letterSpacing="tight">
                Third-rate hand painting
              </Text>
              <Text fontSize={['13px', '16px']}>Artist</Text>
            </Box>
            <Box textAlign="center" m="10px">
              <Img src="/static/img/team_2.webp" h="250px" />
              <Text fontSize={['20px', '24px']}>Bentlee</Text>
              <Text fontSize={['13px', '16px']}>
                Dev {`&`} Community Manager
              </Text>
            </Box>
            <Box textAlign="center" m="10px">
              <Img src="/static/img/team_3.webp" h="250px" />
              <Text fontSize={['20px', '24px']}>{`//`}yuu</Text>
              <Text fontSize={['13px', '16px']}>Dev</Text>
            </Box>
            <Box textAlign="center" m="10px">
              <Img src="/static/img/team_4.webp" h="250px" />
              <Text fontSize={['20px', '24px']}>Rainforest</Text>
              <Text fontSize={['13px', '16px']}>Community Manager</Text>
            </Box>
          </Flex>
        </Container>
      </VStack>
    </>
  );
};

export default withPublicLayout(Home);
