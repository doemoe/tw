import React, {memo, useEffect, useState} from 'react';
import NextHead from 'next/head';
import {withPublicLayout} from 'layouts';
import {
  Skeleton,
  VStack,
  Button,
  Text,
  Link,
  Alert,
  Img,
  Box,
  HStack,
  toast,
  useToast,
} from '@chakra-ui/react';

import {
  useConnect,
  useAccount,
  useNetwork,
  useContractWrite,
  useSigner,
  useBalance,
} from 'wagmi';
import {useMediaQuery} from '@chakra-ui/react';
import {ethers} from 'ethers';
import TestABI from 'abis/testABI';
import {MintState} from '../utils/states';
import {supportedChain, contractAddress} from 'utils/constants';
import MintInput from '@/components/MintInput';
import styles from './mint.module.scss';

const Mint = memo(() => {
  const toast = useToast();
  const [isMobile] = useMediaQuery('(max-width: 1024px)');
  const [{data: connectData, error: connectError}, connect] = useConnect();
  const [
    {data: accountData, error: accountError, loading: accountLoading},
    disconnect,
  ] = useAccount();
  const [
    {data: networkData, error: networkError, loading: networkLoading},
    switchNetwork,
  ] = useNetwork();
  const [signerData] = useSigner();
  const signer = signerData.data;
  const [mintState, setMintState] = useState<MintState>({
    minting: false,
    amount: 0,
  });

  useEffect(() => {
    // console.log(connectData);
    // console.log(networkData);
    if (connectData.connected && supportedChain[`${networkData.chain?.id}`]) {
      setMintState({
        minting: false,
        amount: supportedChain[`${networkData.chain.id}`].price,
      });
    }
  }, [connectData.connected, networkData.chain?.id]);

  const [value, setValue] = useState(1);
  const handleChange = (e) => {
    let val = Number(e.target.value.replace(/[^\d]/, ''));
    if (val > 255) val = 255;
    setValue(val);
  };
  const handleIncrement = (e) => setValue(value + 1 > 255 ? 255 : value + 1);
  const handleDecrement = (e) => setValue(value - 1 < 1 ? 1 : value - 1);

  const [, mint] = useContractWrite(
    {
      addressOrName: contractAddress,
      contractInterface: TestABI,
      signerOrProvider: signer,
    },
    'mint',
    {
      args: [value],
      overrides: {
        value: ethers.utils.parseUnits(`${mintState.amount}`).mul(value),
        // value: ethers.utils.parseUnits("0.001").mul(value),
      },
    },
  );

  if (isMobile) {
    return (
      <>
        <NextHead>
          <title>Titan World | Mint</title>
          <meta
            property="og:title"
            content="Titan World | Mint"
            key="minttitle"
          />
        </NextHead>
        <Text margin="0 auto">Minimum width is 1024px.</Text>
      </>
    );
  }
  if (!connectData.connected) {
    return (
      <>
        <NextHead>
          <title>Titan World | Mint</title>
          <meta property="og:title" content="Titan World | Mint" key="title" />
          <meta
            property="twitter:title"
            content="Titan World | Mint"
            key="twtitle"
          />
        </NextHead>
        <Skeleton height="500px" />
      </>
    );
  } else if (!supportedChain[`${networkData.chain.id}`]) {
    return (
      <>
        <NextHead>
          <title>Titan World | Mint</title>
          <meta property="og:title" content="Titan World | Mint" key="title" />
          <meta
            property="twitter:title"
            content="Titan World | Mint"
            key="twtitle"
          />
        </NextHead>
        <Text margin="0 auto">Current chain is not supported.</Text>
      </>
    );
  }
  return (
    <>
      <NextHead>
        <title>Titan World | Mint</title>
        <meta
          property="og:title"
          content="Titan World | Mint"
          key="minttitle"
        />
        <meta
          property="twitter:title"
          content="Titan World | Mint"
          key="twtitle"
        />
      </NextHead>
      <VStack w="full" spacing={3}>
        <Text
          as="h1"
          fontFamily="Segoe UI"
          fontSize="2rem"
          lineHeight="1"
          fontWeight="500"
          textAlign="center"
        >
          You are now on{' '}
          <Text
            fontFamily="Segoe UI"
            fontSize="4rem"
            my="10px"
            fontWeight="700"
            letterSpacing="-.025em"
            color={`${supportedChain[`${networkData.chain.id}`].color}`}
          >
            {`${supportedChain[`${networkData.chain.id}`].name}`}
          </Text>
        </Text>
        <MintInput
          confirming={mintState.confirming}
          minting={mintState.minting}
          value={value}
          handleChange={handleChange}
          handleIncrement={handleIncrement}
          handleDecrement={handleDecrement}
        />
        <Text>
          Total cost: {`${(mintState.amount * value).toFixed(2)}`}{' '}
          {`${supportedChain[`${networkData.chain.id}`].symbol}`}
        </Text>
        <Box className={styles.mintImgWrapper}>
          <button
            disabled={mintState.confirming || mintState.minting}
            onClick={async (e) => {
              e.preventDefault();
              setMintState({...mintState, confirming: true, minting: false});
              try {
                const result = await mint();
                if (result.error) {
                  console.log(result.error);
                  setMintState({
                    ...mintState,
                    confirming: false,
                    minting: false,
                  });
                  toast({
                    title: 'Error',
                    description:
                      result.error.message == 'Internal JSON-RPC error.'
                        ? 'Internal JSON-RPC error. Please recheck your balance.'
                        : result.error.message,
                    position: 'top-right',
                    status: 'error',
                    duration: 3000,
                    isClosable: true,
                  });
                  return;
                }
                // Wallet confirmed
                setMintState({
                  ...mintState,
                  confirming: false,
                  minting: true,
                  hash: result.data.hash,
                });
                const receipt = await result.data.wait();
                if (receipt.status === 1) {
                  // success
                  const txHash = receipt.logs[receipt.logs.length - 1];
                  console.log(receipt);
                  toast({
                    title: 'Success',
                    description: 'Successfully minted.',
                    position: 'top-right',
                    status: 'success',
                    duration: 3000,
                    isClosable: true,
                  });
                  setMintState({...mintState, minting: false, hash: undefined});
                } else {
                  setMintState({
                    ...mintState,
                    error: new Error('Something wrong with the receipt'),
                    hash: undefined,
                    minting: false,
                  });
                  console.log('mint error');
                  toast({
                    title: 'Error',
                    description: 'Something wrong with the receipt',
                    position: 'top-right',
                    status: 'error',
                    duration: 3000,
                    isClosable: true,
                  });
                  return;
                }
              } catch (e) {
                const error = e as Error;
                console.log(error);
              }
            }}
          >
            <Img
              src="/static/img/mint.webp"
              h="125px"
              opacity={0.8}
              transition="all .5s ease"
              _hover={{opacity: 1, transform: 'scale(1.01)'}}
            />
          </button>
        </Box>
        <Box h="3rem" textAlign="center">
          <Text>
            Collecting the hand of Titan is the first step to resuscitate Titan.
          </Text>
          <Text>
            Adventurers who own the hand of Titan can open the seal of Titan
            {`'`}s weapons and armors more easily.
          </Text>
        </Box>
        <HStack spacing={3}>
          <Img
            src="/static/mintBackground/hand_1.webp"
            className={styles.mintBgImg}
          />
          <Img
            src="/static/mintBackground/hand_2.webp"
            className={styles.mintBgImg}
          />
          <Img
            src="/static/mintBackground/hand_3.webp"
            className={styles.mintBgImg}
          />
          <Img
            src="/static/mintBackground/hand_4.webp"
            className={styles.mintBgImg}
          />
          <Img
            src="/static/mintBackground/hand_5.webp"
            className={styles.mintBgImg}
          />
        </HStack>
      </VStack>
    </>
  );
});

export default withPublicLayout(Mint);
