// import type {AppProps} from 'next/app';
import NextHead from 'next/head';

import {Provider, chain, defaultChains} from 'wagmi';
import {InjectedConnector} from 'wagmi/connectors/injected';
import {WalletConnectConnector} from 'wagmi/connectors/walletConnect';

import {ChakraProvider} from '@chakra-ui/react';

import 'styles/globals.css';

import override from 'styles/override';

// API key for Ethereum node
// Two popular services are Infura (infura.io) and Alchemy (alchemy.com)
const infuraId = process.env.INFURA_ID;

// Chains for connectors to support
const chains = defaultChains;

// Set up connectors
const connectors = ({chainId}) => {
  const rpcUrl =
    chains.find((x) => x.id === chainId)?.rpcUrls?.[0] ??
    chain.mainnet.rpcUrls[0];
  return [
    new InjectedConnector({
      chains,
      options: {shimDisconnect: true},
    }),
    // new WalletConnectConnector({
    //   options: {
    //     infuraId,
    //     qrcode: true,
    //   },
    // }),
  ];
};

function MyApp({Component, pageProps, err}): JSX.Element {
  const getLayout = Component.getLayout || ((page) => page);
  return (
    <ChakraProvider theme={override}>
      <Provider autoConnect connectors={connectors}>
        <NextHead>
          <meta
            content="We yearn for Titan's strength, we search for the king of Titans. 0mnichain NFT 0n @LayerZero_Labs."
            name="description"
          ></meta>
          <meta
            content="We yearn for Titan's strength, we search for the king of Titans. 0mnichain NFT 0n @LayerZero_Labs."
            property="og:description"
          ></meta>
          <meta
            content="We yearn for Titan's strength, we search for the king of Titans. 0mnichain NFT 0n @LayerZero_Labs."
            property="twitter:description"
          ></meta>
          <meta property="og:type" content="website"></meta>
        </NextHead>
        {/* <Component {...pageProps} /> */}
        {getLayout(<Component {...pageProps} err={err} />)}
      </Provider>
    </ChakraProvider>
  );
}

export default MyApp;
