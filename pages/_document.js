import {ColorModeScript} from '@chakra-ui/react';
import NextDocument, {Html, Head, Main, NextScript} from 'next/document';
import override from '../styles/override';

export default class Document extends NextDocument {
  render() {
    return (
      <Html
        lang="en"
        className="scroll-smooth"
        style={{scrollBehavior: 'smooth'}}
      >
        <Head />
        <body>
          {/* 👇 Here's the script */}
          <ColorModeScript
            initialColorMode={override.config.initialColorMode}
          />
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
