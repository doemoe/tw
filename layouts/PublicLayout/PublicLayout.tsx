import React from 'react';

import {Stack, Container} from '@chakra-ui/react';

import Header from './Header';
import Footer from './Footer';

const PublicLayout = (props): JSX.Element => {
  const {children} = props;

  return (
    <Container maxW="full">
      <Header />
      <Stack>{children}</Stack>
      <Footer />
    </Container>
  );
};

export default PublicLayout;
