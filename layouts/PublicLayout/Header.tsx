import React, {memo, useEffect, useState} from 'react';

import ConnectWallet from '@/components/ConnectWallet';
import Navigation from './Navigation';
import MobileNavigation from './MobileNavigation';
import { Img, textDecoration, useMediaQuery } from '@chakra-ui/react';

import NextLink from 'next/link';
import {Box, Stack, Button, Heading, Text, Flex, Link} from '@chakra-ui/react';
import styles from './Header.module.scss';

const Header = memo((props) => {
  const [isMobile] = useMediaQuery('(max-width: 1024px)');
  
  return (
    <Flex
      as="nav"
      align="center"
      wrap="wrap"
      h="72px"
      zIndex={100}
      bg="#0a0a0a"
      color="white"
      {...props}
    >

      <Box className={styles.LeftSection}>
        <NextLink href='/' passHref>
          <Link _hover={{textDecoration: 'none'}}>
            <Text fontSize="25px" fontWeight='700' mr="15px">
              TitanWorld
            </Text>
          </Link>
        </NextLink>
      </Box>
      <Box className={styles.MiddleSection}>
        {isMobile ? <></> : <Navigation />}
      </Box>

      <Box className={styles.RightSection}>
        {isMobile ? <MobileNavigation /> : <ConnectWallet isMobile={isMobile} />}
      </Box>
    </Flex>
  );
});

export default Header;
