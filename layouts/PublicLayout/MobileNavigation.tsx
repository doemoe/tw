import {ReactElement, useState} from 'react';
import NextLink from 'next/link';
import {Stack, Text, Link, Box} from '@chakra-ui/react';

import routes from 'utils/routes';

import styles from './MobileNavigation.module.scss';

const MobileNavigation = (): ReactElement => {
  const [isOpen, setOpen] = useState(false);
  return (
    <Box className={styles.NavLinksContainer}>
      <Box className={styles.LinkItem}>
        <NextLink href={routes.market} passHref>
          <Link className={styles.Link}>MarketPlaces</Link>
        </NextLink>
      </Box>
     
    </Box>
  );
};

export default MobileNavigation;
