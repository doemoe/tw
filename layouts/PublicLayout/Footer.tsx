import {Box, Link, Img, Text} from '@chakra-ui/react';
import React from 'react';

const Footer = () => {
  // Separate components for each section
  return (
    <>
      <Box mt={{base: '100px'}}>
        <Box
          maxW={{base: '100px'}}
          mx="auto"
          d="flex"
          alignItems="center"
          justifyContent="space-between"
        >
          <Link
            href="https://twitter.com/realTitanWorld"
            key="twitter"
            isExternal
          >
            <Img src="/static/twitter.svg" h="1.5rem"></Img>
          </Link>

          <Link href="https://discord.gg/PrXvYJbe4G" key="discord" isExternal>
            <Img src="/static/discord.svg" h="1.5rem"></Img>
          </Link>
        </Box>
      </Box>
      <Box
        d="flex"
        mx="auto"
        mt="16px"
        flexDirection={{base: 'column'}}
        alignItems={{base: 'center'}}
      >
        <Text color="#fff" fontWeight="400" fontSize="11px" opacity="0.69">
          © 2022 Titan World
        </Text>
      </Box>
    </>
  );
};

export default Footer;
