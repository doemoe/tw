import {ReactElement} from 'react';
import NextLink from 'next/link';
import {Stack, Text, Link, Box} from '@chakra-ui/react';


import routes from 'utils/routes';

import styles from './Navigation.module.scss';

const Navigation = (): ReactElement => {

  return (<>
    <Box className={styles.NavSection}>
      <Box className={styles.NavLinksContainer}>
        <Box as='ul' className={styles.LinksWrapper}>
          <Box as='li' className={styles.LinkItem}>
            <NextLink href={routes.story} passHref>
              <Link className={styles.Link}>Story</Link>
            </NextLink>
          </Box>
          <Box as='li' className={styles.LinkItem}>
            <NextLink href={routes.roadmap} passHref>
              <Link className={styles.Link}>Roadmap</Link>
            </NextLink>
          </Box>
          <Box as='li' className={styles.LinkItem}>
            <Link href={routes.faq} className={styles.Link}>FAQ</Link>
          </Box>
          <Box as='li' className={styles.LinkItem}>
            <NextLink href={routes.team} passHref>
              <Link className={styles.Link}>Team</Link>
            </NextLink>
          </Box>
          <Box as='li' className={styles.LinkItem}>
            <NextLink href={routes.mint} passHref>
              <Link className={styles.Link}>Mint</Link>
            </NextLink>
          </Box>
          <Box as='li' className={styles.LinkItem}>
            <NextLink href={routes.market} passHref>
              <Link className={styles.Link}>Marketplaces</Link>
            </NextLink>
          </Box>
        </Box>
      </Box>
    </Box>

    </>
  );
};

export default Navigation;
