// 1. import `extendTheme` function
import {extendTheme, Theme, ThemeConfig} from '@chakra-ui/react';

// 2. Add your color mode config
// const theme: ThemeConfig = {
//   initialColorMode: 'dark',
//   useSystemColorMode: false,
// }

const newTheme: ThemeConfig = extendTheme({
  config: {initialColorMode: 'dark'},
  colors: {
    gray: {
      800: '#0a0a0a',
    },
  },
});

// 3. extend the theme
// const override = extendTheme({ theme })

export default newTheme;
